// ==UserScript==
// @name         Make all JB icons active 2
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  try to take over the world!
// @author       Juliano Polito jpolito@salesforce.com
// @match        https://*.marketingcloudapps.com/*
// @grant        none
// ==/UserScript==

let observer = new MutationObserver((mutations) => {
   mutations.forEach((mutation) => {
       //console.log("mut: ", mutation);
      if (!mutation.addedNodes) return

      for (let i = 0; i < mutation.addedNodes.length; i++) {
          // do things to your newly added nodes here
          let node = mutation.addedNodes[i];
          //console.log("mut class: ", node);

          try{
              if(node.getAttribute("class").includes("icon-background")){
                  var ancestor1 = node.closest('div.activity-icon-orange');
                  if(ancestor1 != null){
                      node.setAttribute("style","background: #ED8B00 !important");
                  }else{
                      //console.log("addedNode: ", node);
                      node.setAttribute("style","background: #4DC6BD !important");
                  }

              }if(node.getAttribute("class").includes("custom")){
                  var doc = node;
                  var notes = null;
                  console.log("list: ", node);
                  for (var ii = 0; ii < doc.childNodes.length; ii++) {
                      console.log("out: ", doc.childNodes[ii].className);
                      if (doc.childNodes[ii].className.includes("icon-background")) {
                          console.log("in: ", doc.childNodes[ii].className);

                          doc.childNodes[ii].setAttribute("style","background: #4DC6BD !important");
                          break;
                      }
                  }
              }
          }catch{}
      }
   })
})

observer.observe(document.body, {
   childList: true,
   subtree: true,
   attributes: true,
   characterData: false
})
