# README #
 
### What is this ###

* Tampermonkey script for making grey icons in journey builder look setup and green
* Version 0.2 - please report any bugs
* Author: Juliano Polito (jpolito@salesforce.com)


### How do I get set up? ###

* Just import the script into tampermonkey and enable it.
* No need to set anything else in the script

